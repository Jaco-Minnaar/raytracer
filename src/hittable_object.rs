use crate::hittable::Hittable;

#[derive(Clone)]
pub struct HittableList {
    pub objects: Vec<Box<Hittable>>,
}

impl HittableList {
    pub fn new() -> Self {
        Self { objects: vec![] }
    }

    pub fn clear(&mut self) {
        self.objects.clear();
    }

    pub fn add(&mut self, object: Box<Hittable>) {
        self.objects.push(object);
    }
}
