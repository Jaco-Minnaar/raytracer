use crate::{
    hittable_object::HittableList,
    material::Material,
    ray::Ray,
    vec3::{dot, Point3, Vec3},
};

#[derive(Clone, Copy)]
pub struct HitRecord {
    pub p: Point3,
    pub normal: Vec3,
    pub material: Material,
    pub t: f64,
    pub front_face: bool,
}

impl HitRecord {
    pub fn set_face_normal(&mut self, r: &Ray, outward_normal: &Vec3) {
        self.front_face = dot(&r.direction, &outward_normal) < 0.0;
        self.normal = if self.front_face {
            outward_normal.clone()
        } else {
            -outward_normal.clone()
        }
    }
}

#[derive(Clone)]
pub enum Hittable {
    Sphere {
        center: Point3,
        radius: f64,
        material: Material,
    },
    HittableList {
        list: HittableList,
    },
}

impl Hittable {
    pub fn hit(&self, r: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        match self {
            Hittable::Sphere {
                center,
                radius,
                material,
            } => self.sphere_hit(r, t_min, t_max, &center, *radius, &material),
            Hittable::HittableList { list } => {
                let mut hit_rec: Option<HitRecord> = None;
                let mut closest_so_far = t_max;

                for i in 0..list.objects.len() {
                    let object = &list.objects[i];
                    if let Some(temp_rec) = object.hit(&r, t_min, closest_so_far) {
                        closest_so_far = temp_rec.t;
                        hit_rec = Some(temp_rec);
                    }
                }

                hit_rec
            }
        }
    }

    fn sphere_hit(
        &self,
        r: &Ray,
        t_min: f64,
        t_max: f64,
        center: &Point3,
        radius: f64,
        material: &Material,
    ) -> Option<HitRecord> {
        let oc = r.origin - *center;
        let a = r.direction.length_squared();
        let half_b = dot(&oc, &r.direction);
        let c = oc.length_squared() - radius * radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant < 0.0 {
            return None;
        }
        let sqrtd = discriminant.sqrt();
        let mut root = (-half_b - sqrtd) / a;
        if root < t_min || t_max < root {
            root = (-half_b + sqrtd) / a;
            if root < t_min || t_max < root {
                return None;
            }
        }
        let mut record = HitRecord {
            t: root,
            p: r.at(root),
            normal: (r.at(root) - *center) / radius,
            material: *material,
            front_face: true,
        };
        let outward_normal = (record.p - *center) / radius;
        record.set_face_normal(&r, &outward_normal);
        return Some(record);
    }
}
