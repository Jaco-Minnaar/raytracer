use hittable::Hittable;
use hittable_object::HittableList;
use material::Material;
use ray::Ray;
use vec3::{unit_vector, Color, Point3};

pub mod camera;
pub mod color;
pub mod hittable;
pub mod hittable_object;
pub mod material;
pub mod ray;
pub mod vec3;

pub static PI: f64 = std::f64::consts::PI;
pub static INFINITY: f64 = f64::INFINITY;

pub fn ray_color(ray: &Ray, world: &Hittable, depth: u32) -> Color {
    if depth <= 0 {
        return Color::new(0.0, 0.0, 0.0);
    }

    if let Some(rec) = world.hit(&ray, 0.001, INFINITY) {
        return if let Some(scatter_result) = rec.material.scatter(&ray, &rec) {
            scatter_result.attenuation * ray_color(&scatter_result.scattered_ray, world, depth - 1)
        } else {
            Color::new(0.0, 0.0, 0.0)
        };
    }

    let unit_direction = unit_vector(&ray.direction);
    let t = 0.5 * (unit_direction.y + 1.0);

    (1.0 - t) * Color::new(1.0, 1.0, 1.0) + t * Color::new(0.5, 0.7, 1.0)
}

pub fn degrees_to_radians(degrees: f64) -> f64 {
    degrees * PI / 180.0
}

pub fn random_f64() -> f64 {
    let random_num = rand::random::<f64>();

    random_num
}

pub fn random_f64_in_range(min: f64, max: f64) -> f64 {
    min + (max - min) * random_f64()
}

pub fn clamp(x: f64, min: f64, max: f64) -> f64 {
    if x < min {
        min
    } else if x > max {
        max
    } else {
        x
    }
}

pub fn random_scene() -> HittableList {
    let mut world = HittableList::new();

    let ground_material = Material::Lambertian {
        albedo: Color::new(0.5, 0.5, 0.5),
    };
    world.add(Box::new(Hittable::Sphere {
        center: Point3::new(0.0, -1000.0, 0.0),
        radius: 1000.0,
        material: ground_material,
    }));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = random_f64();
            let center = Point3::new(
                a as f64 + 0.9 * random_f64(),
                0.2,
                b as f64 + 0.9 * random_f64(),
            );

            if (center - Point3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                let sphere_material = if choose_mat < 0.8 {
                    let albedo = Color::random() * Color::random();

                    Material::Lambertian { albedo }
                } else if choose_mat < 0.95 {
                    let albedo = Color::random_in_range(0.5, 1.0);
                    let fuzz = random_f64_in_range(0.0, 0.5);

                    Material::Metal { albedo, fuzz }
                } else {
                    Material::Dielectric {
                        refractive_index: 1.5,
                    }
                };

                world.add(Box::new(Hittable::Sphere {
                    center,
                    radius: 0.2,
                    material: sphere_material,
                }));
            }
        }
    }

    let mat1 = Material::Dielectric {
        refractive_index: 1.5,
    };
    world.add(Box::new(Hittable::Sphere {
        center: Point3::new(0.0, 1.0, 0.0),
        radius: 1.0,
        material: mat1,
    }));

    let mat2 = Material::Lambertian {
        albedo: Color::new(0.4, 0.2, 0.1),
    };
    world.add(Box::new(Hittable::Sphere {
        center: Point3::new(-4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat2,
    }));

    let mat3 = Material::Metal {
        albedo: Color::new(0.7, 0.6, 0.5),
        fuzz: 5.0,
    };
    world.add(Box::new(Hittable::Sphere {
        center: Point3::new(4.0, 1.0, 0.0),
        radius: 1.0,
        material: mat3,
    }));

    world
}
