use crate::{
    hittable::HitRecord,
    random_f64,
    ray::Ray,
    vec3::{dot, random_in_unit_sphere, random_unit_vector, reflect, refract, unit_vector, Color},
};

pub struct ScatterResult {
    pub scattered_ray: Ray,
    pub attenuation: Color,
}

#[derive(Clone, Copy)]
pub enum Material {
    Lambertian { albedo: Color },
    Metal { albedo: Color, fuzz: f64 },
    Dielectric { refractive_index: f64 },
}

impl Material {
    pub fn scatter(&self, r_in: &Ray, record: &HitRecord) -> Option<ScatterResult> {
        match self {
            &Material::Lambertian { albedo } => lambertian_scatter(record, albedo),
            &Material::Metal { albedo, fuzz } => metal_scatter(r_in, record, fuzz, albedo),
            &Material::Dielectric { refractive_index } => {
                dielectric_scatter(record, refractive_index, r_in)
            }
        }
    }
}

fn dielectric_scatter(
    record: &HitRecord,
    refractive_index: f64,
    r_in: &Ray,
) -> Option<ScatterResult> {
    let attenuation = Color::new(1.0, 1.0, 1.0);
    let refraction_ratio = if record.front_face {
        1.0 / refractive_index
    } else {
        refractive_index
    };

    let unit_direction = unit_vector(&r_in.direction);
    let dot_product = dot(&-unit_direction, &record.normal);
    let cos_theta = if dot_product < 1.0 { dot_product } else { 1.0 };
    let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

    let cannot_refract = refraction_ratio * sin_theta > 1.0;
    let direction = if cannot_refract || reflectance(cos_theta, refraction_ratio) > random_f64() {
        reflect(&unit_direction, &record.normal)
    } else {
        refract(&unit_direction, &record.normal, refraction_ratio)
    };

    let scattered_ray = Ray::new(record.p, direction);
    Some(ScatterResult {
        scattered_ray,
        attenuation,
    })
}

fn metal_scatter(
    r_in: &Ray,
    record: &HitRecord,
    fuzz: f64,
    albedo: crate::vec3::Vec3,
) -> Option<ScatterResult> {
    let reflected = reflect(&r_in.direction, &record.normal);
    let scatter_result = ScatterResult {
        scattered_ray: Ray::new(record.p, reflected + fuzz * random_in_unit_sphere()),
        attenuation: albedo,
    };
    if dot(&scatter_result.scattered_ray.direction, &record.normal) > 0.0 {
        Some(scatter_result)
    } else {
        None
    }
}

fn lambertian_scatter(record: &HitRecord, albedo: crate::vec3::Vec3) -> Option<ScatterResult> {
    let mut scatter_direction = record.normal + random_unit_vector();
    if scatter_direction.near_zero() {
        scatter_direction = record.normal;
    }
    let scatter_result = ScatterResult {
        scattered_ray: Ray::new(record.p, scatter_direction),
        attenuation: albedo,
    };
    Some(scatter_result)
}

fn reflectance(cosine: f64, refractive_index: f64) -> f64 {
    let mut r0 = (1.0 - refractive_index) / (1.0 + refractive_index);
    r0 = r0 * r0;

    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}
