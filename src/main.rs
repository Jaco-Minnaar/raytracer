use std::{
    collections::HashMap,
    io,
    sync::{mpsc::channel, Arc, Mutex},
};

use raytracer::{
    camera::Camera,
    color::write_color,
    hittable::Hittable,
    random_f64, random_scene, ray_color,
    vec3::{Color, Point3, Vec3},
};
use threadpool::ThreadPool;

fn main() {
    // Image
    const ASPECT_RATIO: f64 = 3.0 / 2.0;
    const IMAGE_WIDTH: usize = 1200;
    const IMAGE_HEIGHT: usize = (IMAGE_WIDTH as f64 / ASPECT_RATIO) as usize;
    let samples_per_pixel: u32 = 32;
    let max_depth = 8;

    let world = random_scene();

    // Camera
    let look_from = Point3::new(13.0, 2.0, 3.0);
    let look_at = Point3::new(0.0, 0.0, 0.0);
    let v_up = Vec3::new(0.0, 1.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;

    let camera = Camera::new(
        &look_from,
        &look_at,
        &v_up,
        20.0,
        ASPECT_RATIO,
        aperture,
        dist_to_focus,
    );

    // Render
    println!("P3");
    println!("{} {}", IMAGE_WIDTH, IMAGE_HEIGHT);
    println!("255");

    let map: HashMap<usize, HashMap<usize, Color>> = HashMap::new();
    let map_arc = Arc::new(Mutex::new(map));
    // let world_arc = Arc::new(Mutex::new(world));
    // let camera_arc = Arc::new(Mutex::new(camera));
    let interactive_world = Hittable::HittableList { list: world };

    let workers = num_cpus::get();
    let jobs = IMAGE_HEIGHT;
    eprintln!("Workers: {}, Jobs: {}", workers, jobs);
    let pool = ThreadPool::new(workers);
    let (tx, rx) = channel();

    for j in (0..IMAGE_HEIGHT).rev() {
        let mut row = HashMap::new();
        let map_clone = Arc::clone(&map_arc);
        let world_clone = interactive_world.clone();
        let camera_clone = camera.clone();
        let tx = tx.clone();

        pool.execute(move || {
            for i in 0..IMAGE_WIDTH {
                let mut pixel_color = Color::new(0.0, 0.0, 0.0);
                for _s in 0..samples_per_pixel {
                    let u = (i as f64 + random_f64()) / (IMAGE_WIDTH as f64 - 1.0);
                    let v = (j as f64 + random_f64()) / (IMAGE_HEIGHT as f64 - 1.0);

                    let r = camera_clone.get_ray(u, v);

                    pixel_color += ray_color(&r, &world_clone, max_depth);
                }

                row.insert(i, pixel_color);
            }

            map_clone.lock().unwrap().insert(j, row);
            tx.send(1).unwrap();
        })
    }

    let mut jobs_remaining = jobs;
    for job_done in rx.iter() {
        jobs_remaining -= job_done;

        eprint!("\rScanlines remaining: {} ", jobs_remaining);
        if jobs_remaining <= 0 {
            break;
        }
    }

    eprintln!("Writing results to file...");

    let map = map_arc.lock().unwrap();
    for row_key in (0..IMAGE_HEIGHT).rev() {
        let row = map.get(&row_key).unwrap();
        for value_key in 0..IMAGE_WIDTH {
            let value = row.get(&value_key).unwrap();
            write_color(&mut io::stdout(), value, samples_per_pixel);
        }
    }

    eprintln!("Done.");
}
